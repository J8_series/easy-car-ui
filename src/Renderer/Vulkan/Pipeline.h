#ifndef EASY_CAR_UI_RENDER_ENGINE_PIPELINE_H_
#define EASY_CAR_UI_RENDER_ENGINE_PIPELINE_H_

#include <string>
#include <vector>

#include <vulkan/vulkan.h>

namespace namespace_easy_car_ui
{

class Pipeline
{
public:
    Pipeline();
    ~Pipeline();
    Pipeline(const Pipeline&) = delete;
    Pipeline(const Pipeline&&) = delete;
    Pipeline& operator=(const Pipeline&) = delete;
    Pipeline& operator=(const Pipeline&&) = delete;

    void Initialize(
        const std::string& vertexShaderFile, 
        const std::string& fragmentShaderFile,
        const VkDevice& device,
        const VkRenderPass& renderPass);

    void Deinitialize();

    VkPipeline GetPipeline();
    VkPipelineLayout GetLayout();
    VkDescriptorSetLayout GetDescriptorSetLayout();

private:
    std::vector<char> ReadShader(const std::string& shaderFile);
    VkShaderModule CreateShaderModule(std::vector<char>& code);
    void CreateDescriptorSetLayout();

private:
    std::string m_vertexShaderFile {};
    std::string m_fragmentShaderFile {};
    VkDevice m_device {};
    VkRenderPass m_renderPass {};
    VkPipelineLayout m_pipelineLayout {};
    VkPipeline m_graphicsPipeline {};
    VkDescriptorSetLayout m_descriptorSetLayout {};

};


}


#endif /*EASY_CAR_UI_RENDER_ENGINE_PIPELINE_H_*/