#ifndef EASY_CAR_UI_RENDER_ENGINE_COMMAND_TOOL_H_
#define EASY_CAR_UI_RENDER_ENGINE_COMMAND_TOOL_H_

#include <vulkan/vulkan.h>

#include <string>

namespace namespace_easy_car_ui
{

class CommandTool
{
public:
    CommandTool() = delete;
    ~CommandTool() = delete;
    CommandTool(const CommandTool&) = delete;
    CommandTool(const CommandTool&&) = delete;
    CommandTool& operator=(const CommandTool&) = delete;
    CommandTool& operator=(const CommandTool&&) = delete;

    static VkCommandBuffer BeginSingleTimeCommands(VkDevice device, VkCommandPool commandPool);
    static void EndSingleTimeCommands(VkDevice device,  VkCommandPool commandPool, VkCommandBuffer commandBuffer, VkQueue queue);
};

} /* namespace_easy_car_ui */

#endif /* EASY_CAR_UI_RENDER_ENGINE_MEMORY_TOOL_H_ */