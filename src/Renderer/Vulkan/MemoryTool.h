#ifndef EASY_CAR_UI_RENDER_ENGINE_MEMORY_TOOL_H_
#define EASY_CAR_UI_RENDER_ENGINE_MEMORY_TOOL_H_

#include <vulkan/vulkan.h>

#include <string>

namespace namespace_easy_car_ui
{

class MemoryTool
{
public:
    MemoryTool() = delete;
    ~MemoryTool() = delete;
    MemoryTool(const MemoryTool&) = delete;
    MemoryTool(const MemoryTool&&) = delete;
    MemoryTool& operator=(const MemoryTool&) = delete;
    MemoryTool& operator=(const MemoryTool&&) = delete;

    static void CreateBuffer(
                VkPhysicalDevice physicalDevice,
                VkDevice device,
                VkDeviceSize size,
                VkBufferUsageFlags usage,
                VkMemoryPropertyFlags properties,
                VkBuffer& buffer,
                VkDeviceMemory& bufferMemory);

    static void CreateImage(
                VkPhysicalDevice physicalDevice,
                VkDevice device,
                uint32_t width,
                uint32_t height,
                VkFormat format,
                VkImageTiling tiling,
                VkImageUsageFlags usage,
                VkMemoryPropertyFlags properties,
                VkImage& image,
                VkDeviceMemory& imageMemory);

     static VkImageView CreateImageView(
                VkDevice device,
                VkImage image,
                VkFormat format,
                VkImageAspectFlags aspectFlags);

     static void CopyBuffer(
                VkDevice device,
                VkCommandPool transmissionCommandPool,
                VkQueue transmissionQueue,
                VkBuffer srcBuffer,
                VkBuffer dstBuffer,
                VkDeviceSize size);

private:
    static uint32_t FindMemoryType(
            VkPhysicalDevice physicalDevice,
            uint32_t typeFilter,
            VkMemoryPropertyFlags properties);

};

} /* namespace_easy_car_ui */

#endif /* EASY_CAR_UI_RENDER_ENGINE_MEMORY_TOOL_H_ */