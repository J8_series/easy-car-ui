#ifndef EASY_CAR_UI_RENDER_ENGINE_VULKAN_NODE_H_
#define EASY_CAR_UI_RENDER_ENGINE_VULKAN_NODE_H_

#include <vulkan/vulkan.h>

#include <string>
#include <memory>

#include "Model/Node.h"

namespace namespace_easy_car_ui
{

class VulkanNode
{
public:
    using Ptr = std::shared_ptr<VulkanNode>;

    VulkanNode(
        VkPhysicalDevice physicalDevice,
        VkDevice device,
        VkCommandPool commandPool,
        VkQueue queue);
    ~VulkanNode();
    VulkanNode(const VulkanNode&) = delete;
    VulkanNode(const VulkanNode&&) = delete;
    VulkanNode& operator=(const VulkanNode&) = delete;
    VulkanNode& operator=(const VulkanNode&&) = delete;

    void CreateIndexAndVertexBuffer(const Node& node);
    VkBuffer GetVertexBuffer();
    VkBuffer GetIndexBuffer();
    uint32_t GetNumIndics();

private:
    uint32_t m_numIndics {0};
    VkBuffer m_vertexBuffer {};
    VkDeviceMemory m_vertexBufferMemory {};
    VkBuffer m_indexBuffer {};
    VkDeviceMemory m_indexBufferMemory {};

    VkPhysicalDevice m_physicalDevice;
    VkDevice m_device;
    VkCommandPool m_commandPool;
    VkQueue m_queue;

};

} /* namespace_easy_car_ui */


#endif /*EASY_CAR_UI_RENDER_ENGINE_VULKAN_NODE_H_*/