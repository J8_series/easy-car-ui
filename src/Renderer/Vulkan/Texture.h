#ifndef EASY_CAR_UI_RENDER_ENGINE_TEXTURE_H_
#define EASY_CAR_UI_RENDER_ENGINE_TEXTURE_H_

#include <vulkan/vulkan.h>

#include <string>
#include <memory>

namespace namespace_easy_car_ui
{

class Texture
{
public:
    using Ptr = std::shared_ptr<Texture>;

    Texture(
        VkPhysicalDevice physicalDevice,
        VkDevice device,
        VkCommandPool commandPool,
        VkQueue queue);
    ~Texture();
    Texture(const Texture&) = delete;
    Texture(const Texture&&) = delete;
    Texture& operator=(const Texture&) = delete;
    Texture& operator=(const Texture&&) = delete;

    void SetImagePath(const std::string& path);
    VkImageView GetImageView();
    VkSampler GetSampler();

private:
    void Reset();
    void LoadImage();
    void CopyBufferToImage(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height);
    void TransitionImageLayout(VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout);
    void CreateTextureSampler();
    bool HasStencilComponent(VkFormat format);

public:
    VkPhysicalDevice m_physicalDevice {};
    VkDevice m_device {};
    std::string m_imagePath {};
    int32_t m_texWidth {0};
    int32_t m_texHeight {0};
    int32_t m_texChannels {0};
    VkDeviceSize m_texSize {0};
    VkCommandPool m_commandPool {};
    VkQueue m_queue {};
    VkImage m_textureImage {};
    VkImageView m_textureImageView {};
    VkDeviceMemory m_textureImageMemory {};
    VkSampler m_sampler{};
    
};

} /* namespace_easy_car_ui */


#endif /*EASY_CAR_UI_RENDER_ENGINE_TEXTURE_H_*/