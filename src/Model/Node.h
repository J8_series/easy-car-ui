#pragma once

#include <vector>
#include <string>

#include "Vertex.h"
#include "ModelViewProj.h"

namespace namespace_easy_car_ui
{

class Node
{
public:
    std::vector<Vertex> m_vertes {};
    std::vector<uint32_t> m_indices {};
    std::string m_texturePath {};
    std::string m_id {};
    glm::mat4 model{};
public:
    Node();
    ~Node();
};

} /* namespace namespace_easy_car_ui */
