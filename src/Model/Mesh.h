#pragma once

#include <map>
#include <string>
#include <memory>
#include <vector>

#include "Vertex.h"

namespace namespace_easy_car_ui
{

class Mesh
{
public:
    using Ptr = std::shared_ptr<Mesh>;

    Mesh();
    ~Mesh();

    void PrintInfo();

    std::string m_name{""};
    std::vector<Vertex> m_vertexes {};
    std::vector<uint32_t> m_vertexesIndices {};

};

} /* namespace namespace_easy_car_ui */