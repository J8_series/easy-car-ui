#pragma once

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace namespace_easy_car_ui
{

struct Vertex
{
    glm::vec3 m_position{0.0f};
    glm::vec3 m_color{1.0f};
    glm::vec2 m_texCoord{0.0f};

    constexpr static uint32_t GetAttributeCount()
    {
        return 3;
    }
};

} /* namespace namespace_easy_car_ui */
