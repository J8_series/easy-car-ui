#pragma once

#include "Model/Mesh.h"

namespace namespace_easy_car_ui
{

class Part
{
public:
    using Ptr = std::shared_ptr<Part>;

    Part();
    ~Part();

    void PrintInfo();

    std::string m_name{""};
    glm::mat4 m_transformMatrix {1.0f};
    std::vector<Mesh::Ptr> m_meshes {};
    std::map<std::string, Part::Ptr> m_children {};
};

} /* namespace namespace_easy_car_ui */