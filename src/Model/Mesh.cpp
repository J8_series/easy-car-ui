#include <iostream>

#include "Model/Mesh.h"

namespace namespace_easy_car_ui
{

Mesh::Mesh()
{
    
}

Mesh::~Mesh()
{
    m_vertexes.clear();
    m_vertexesIndices.clear();
}

void Mesh::PrintInfo()
{
    std::cout << "Mesh Name is " << m_name << std::endl;

    std::cout << "Vertexes" << std::endl;

    for (auto one : m_vertexes)
    {
        std::cout << " X " << one.m_position.x << " Y " << one.m_position.y << " Z " << one.m_position.z << std::endl;
    }
}

}