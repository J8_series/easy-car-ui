#include "Model/Model.h"

namespace namespace_easy_car_ui
{

Model::Model()
{
    
}

Model::~Model()
{
    m_rootPart.reset();
}

void Model::PrintInfo()
{
    if (nullptr != m_rootPart.get())
    {
        m_rootPart->PrintInfo();
    }
}

} /*namespace namespace_easy_car_ui*/