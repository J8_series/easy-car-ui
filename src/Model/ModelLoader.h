#pragma once

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "Model/Model.h"

namespace namespace_easy_car_ui
{

class ModelLoader
{
private:
    ModelLoader();
    ModelLoader(const ModelLoader&) = delete;
    ModelLoader(const ModelLoader&&) = delete;
    ModelLoader& operator=(const ModelLoader&) = delete;
    ModelLoader& operator=(const ModelLoader&&) = delete;

public:
    static ModelLoader* GetInstance();
    ~ModelLoader();
    Model::Ptr LoadModel(const std::string& filePath);

private:
    void LoadOneNode(const aiScene *scene, aiNode* node, Part::Ptr part);
    void CopyMeshesToVertexes(const aiScene *scene, aiNode* node, Part::Ptr part);
    void CopyOneMesh(const aiMesh* aiMesh, Mesh::Ptr mesh);
    void TransformMatrix4(aiMatrix4x4& aiMat4, glm::mat4& glmMat4);
    

};

} /* namespace namespace_easy_car_ui */