#include <iostream>

#include "Model/Part.h"

namespace namespace_easy_car_ui
{

Part::Part()
{
}

Part::~Part()
{
    m_meshes.clear();
    m_children.clear();
}

void Part::PrintInfo()
{
    std::cout << "=============== Part " << m_name << " START ============" << std::endl;
    std::cout << "--------------- Mesh START-------------" << std::endl;
    for (auto one : m_meshes)
    {
        if (nullptr != one)
        {
            one->PrintInfo();
        }
    }
    std::cout << "--------------- Mesh END -----------" << std::endl;

    std::cout << "--------------- Children START ------------" << std::endl;

    for (auto one : m_children)
    {
        one.second->PrintInfo();
    }

    std::cout << "--------------- Children END -----------" << std::endl;

    std::cout << "=============== Part " << m_name << " END ==============" << std::endl;
}

}