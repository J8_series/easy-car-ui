#pragma once

#include <Model/Part.h>

namespace namespace_easy_car_ui
{

class Model
{
public:
    using Ptr = std::shared_ptr<Model>;

    Model();
    ~Model();

    void PrintInfo();

    Part::Ptr m_rootPart {};
};

} /* namespace namespace_easy_car_ui */